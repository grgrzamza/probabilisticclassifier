﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ProbabilisticClassifier
{
    public class GaussParameters
    {
        public double ArithmeticMean { get; private set; }

        public double StandardDeviation { get; private set; }

        public double Probability { get; private set; }

        public Func<double, double> ProbabilisticFunction => (double x) => ProbabilityDensity(x);        

        public GaussParameters(double mean, double deviation, double probability)
        {
            ArithmeticMean = mean;
            StandardDeviation = deviation;
            Probability = probability;
        }                

        public double ProbabilityDensity(double x)
        {
            double gaussian = 1 / (StandardDeviation * Math.Sqrt(2 * Math.PI)) * Math.Exp(-0.5 * Math.Pow((x - ArithmeticMean) / StandardDeviation, 2));
            return gaussian * Probability;
        }

        public List<double> GetVector(int count)
        {
            var r = new Random();
            List<double> result = new List<double>();
            for (int i = 0; i < count; i++)
            {
                result.Add(r.NextGaussian(ArithmeticMean, StandardDeviation));
            }
            return result;
        }

        public static Tuple<double, double> GetAreas(Tuple<double, double> interval, double step, GaussParameters first, GaussParameters second)
        {
            double start = interval.Item1;
            double stop = interval.Item2;
            double detectionError = 0;
            double falsePositive = 0;
            List<double> range = new List<double>();
            double currentIntervalStop = start;
            Random r = new Random();
            while (currentIntervalStop < stop)
            {
                range.Add(r.NextDouble() * (step) + currentIntervalStop);
                currentIntervalStop += step;
            }
            foreach (double value in range)
            {
                double firstProbabilityResult = first.ProbabilityDensity(value);
                double secondProbabilityResult = second.ProbabilityDensity(value);
                if (firstProbabilityResult > secondProbabilityResult)
                {
                    detectionError += secondProbabilityResult * step;
                }
                else
                {
                    falsePositive += firstProbabilityResult * step;
                }
            }
            return new Tuple<double, double>(detectionError, falsePositive);
        }
    }
}
