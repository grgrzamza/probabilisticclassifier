﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using OxyPlot;
using OxyPlot.Series;

namespace ProbabilisticClassifier
{
    public partial class Form : System.Windows.Forms.Form
    {
        int count = 1000;

        double step = 0.001;

        public Form()
        {
            InitializeComponent();
        }

        private void btnSolve_Click(object sender, EventArgs e)
        {
            try
            {
                double probability = Convert.ToDouble(tbProbability.Text);
                double firstMean = Convert.ToDouble(tbFirstMean.Text);
                double firstDeviation = Convert.ToDouble(tbFirstDeviation.Text);
                double secondMean = Convert.ToDouble(tbSecondMean.Text);
                double secondDeviation = Convert.ToDouble(tbSecondDeviation.Text);
                GaussParameters first = new GaussParameters(firstMean, firstDeviation, probability);
                GaussParameters second = new GaussParameters(secondMean, secondDeviation, 1 - probability);
                IEnumerable<double> vector = first.GetVector(count).Concat(second.GetVector(count));
                double minValue = vector.Min();
                double maxValue = vector.Max();
                Tuple<double, double> results = GaussParameters.GetAreas(new Tuple<double, double>(minValue, maxValue), step, first, second);
                lblErrorDetection.Text = "Вероятность ложной тревоги: " + Environment.NewLine + results.Item1.ToString();
                lblFalsePositive.Text = "Вероятность пропуска ошибки: " + Environment.NewLine + results.Item2.ToString();
                lblSummary.Text = "Вероятность суммарной ошибки: " + Environment.NewLine + (results.Item1 + results.Item2).ToString();
                var model = new PlotModel();
                model.Series.Add(new FunctionSeries(first.ProbabilisticFunction, minValue, maxValue, step));
                model.Series.Add(new FunctionSeries(second.ProbabilisticFunction, minValue, maxValue, step));
                this.plot.Model = model;
            }
            catch (FormatException ex)
            {
                MessageBox.Show("Incorrect input");
            }
        }
    }
}
