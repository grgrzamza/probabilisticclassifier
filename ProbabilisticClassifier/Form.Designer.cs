﻿namespace ProbabilisticClassifier
{
    partial class Form
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.plot = new OxyPlot.WindowsForms.PlotView();
            this.tbProbability = new System.Windows.Forms.TextBox();
            this.lblProbability = new System.Windows.Forms.Label();
            this.lblFirstMean = new System.Windows.Forms.Label();
            this.tbFirstMean = new System.Windows.Forms.TextBox();
            this.lblFirstDeviation = new System.Windows.Forms.Label();
            this.tbFirstDeviation = new System.Windows.Forms.TextBox();
            this.lblSecondMean = new System.Windows.Forms.Label();
            this.tbSecondMean = new System.Windows.Forms.TextBox();
            this.lblSecondDeviation = new System.Windows.Forms.Label();
            this.tbSecondDeviation = new System.Windows.Forms.TextBox();
            this.btnSolve = new System.Windows.Forms.Button();
            this.lblErrorDetection = new System.Windows.Forms.Label();
            this.lblFalsePositive = new System.Windows.Forms.Label();
            this.lblSummary = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // plot
            // 
            this.plot.Dock = System.Windows.Forms.DockStyle.Left;
            this.plot.Location = new System.Drawing.Point(0, 0);
            this.plot.Name = "plot";
            this.plot.PanCursor = System.Windows.Forms.Cursors.Hand;
            this.plot.Size = new System.Drawing.Size(663, 401);
            this.plot.TabIndex = 0;
            this.plot.Text = "plot";
            this.plot.ZoomHorizontalCursor = System.Windows.Forms.Cursors.SizeWE;
            this.plot.ZoomRectangleCursor = System.Windows.Forms.Cursors.SizeNWSE;
            this.plot.ZoomVerticalCursor = System.Windows.Forms.Cursors.SizeNS;
            // 
            // tbProbability
            // 
            this.tbProbability.Location = new System.Drawing.Point(696, 28);
            this.tbProbability.Name = "tbProbability";
            this.tbProbability.Size = new System.Drawing.Size(121, 20);
            this.tbProbability.TabIndex = 1;
            // 
            // lblProbability
            // 
            this.lblProbability.AutoSize = true;
            this.lblProbability.Location = new System.Drawing.Point(696, 9);
            this.lblProbability.Name = "lblProbability";
            this.lblProbability.Size = new System.Drawing.Size(58, 13);
            this.lblProbability.TabIndex = 2;
            this.lblProbability.Text = "Probability:";
            // 
            // lblFirstMean
            // 
            this.lblFirstMean.AutoSize = true;
            this.lblFirstMean.Location = new System.Drawing.Point(696, 63);
            this.lblFirstMean.Name = "lblFirstMean";
            this.lblFirstMean.Size = new System.Drawing.Size(55, 13);
            this.lblFirstMean.TabIndex = 4;
            this.lblFirstMean.Text = "First mean";
            // 
            // tbFirstMean
            // 
            this.tbFirstMean.Location = new System.Drawing.Point(696, 82);
            this.tbFirstMean.Name = "tbFirstMean";
            this.tbFirstMean.Size = new System.Drawing.Size(121, 20);
            this.tbFirstMean.TabIndex = 3;
            // 
            // lblFirstDeviation
            // 
            this.lblFirstDeviation.AutoSize = true;
            this.lblFirstDeviation.Location = new System.Drawing.Point(696, 114);
            this.lblFirstDeviation.Name = "lblFirstDeviation";
            this.lblFirstDeviation.Size = new System.Drawing.Size(72, 13);
            this.lblFirstDeviation.TabIndex = 6;
            this.lblFirstDeviation.Text = "First deviation";
            // 
            // tbFirstDeviation
            // 
            this.tbFirstDeviation.Location = new System.Drawing.Point(696, 133);
            this.tbFirstDeviation.Name = "tbFirstDeviation";
            this.tbFirstDeviation.Size = new System.Drawing.Size(121, 20);
            this.tbFirstDeviation.TabIndex = 5;
            // 
            // lblSecondMean
            // 
            this.lblSecondMean.AutoSize = true;
            this.lblSecondMean.Location = new System.Drawing.Point(696, 168);
            this.lblSecondMean.Name = "lblSecondMean";
            this.lblSecondMean.Size = new System.Drawing.Size(73, 13);
            this.lblSecondMean.TabIndex = 8;
            this.lblSecondMean.Text = "Second mean";
            // 
            // tbSecondMean
            // 
            this.tbSecondMean.Location = new System.Drawing.Point(696, 187);
            this.tbSecondMean.Name = "tbSecondMean";
            this.tbSecondMean.Size = new System.Drawing.Size(121, 20);
            this.tbSecondMean.TabIndex = 7;
            // 
            // lblSecondDeviation
            // 
            this.lblSecondDeviation.AutoSize = true;
            this.lblSecondDeviation.Location = new System.Drawing.Point(693, 221);
            this.lblSecondDeviation.Name = "lblSecondDeviation";
            this.lblSecondDeviation.Size = new System.Drawing.Size(90, 13);
            this.lblSecondDeviation.TabIndex = 10;
            this.lblSecondDeviation.Text = "Second deviation";
            // 
            // tbSecondDeviation
            // 
            this.tbSecondDeviation.Location = new System.Drawing.Point(696, 240);
            this.tbSecondDeviation.Name = "tbSecondDeviation";
            this.tbSecondDeviation.Size = new System.Drawing.Size(121, 20);
            this.tbSecondDeviation.TabIndex = 9;
            // 
            // btnSolve
            // 
            this.btnSolve.Location = new System.Drawing.Point(712, 266);
            this.btnSolve.Name = "btnSolve";
            this.btnSolve.Size = new System.Drawing.Size(83, 26);
            this.btnSolve.TabIndex = 11;
            this.btnSolve.Text = "Go";
            this.btnSolve.UseVisualStyleBackColor = true;
            this.btnSolve.Click += new System.EventHandler(this.btnSolve_Click);
            // 
            // lblErrorDetection
            // 
            this.lblErrorDetection.AutoSize = true;
            this.lblErrorDetection.Location = new System.Drawing.Point(669, 305);
            this.lblErrorDetection.Name = "lblErrorDetection";
            this.lblErrorDetection.Size = new System.Drawing.Size(162, 13);
            this.lblErrorDetection.TabIndex = 12;
            this.lblErrorDetection.Text = "Вероятность ложной тревоги: ";
            // 
            // lblFalsePositive
            // 
            this.lblFalsePositive.AutoSize = true;
            this.lblFalsePositive.Location = new System.Drawing.Point(669, 336);
            this.lblFalsePositive.Name = "lblFalsePositive";
            this.lblFalsePositive.Size = new System.Drawing.Size(166, 13);
            this.lblFalsePositive.TabIndex = 13;
            this.lblFalsePositive.Text = "Вероятность пропуска ошибки:";
            // 
            // lblSummary
            // 
            this.lblSummary.AutoSize = true;
            this.lblSummary.Location = new System.Drawing.Point(669, 365);
            this.lblSummary.Name = "lblSummary";
            this.lblSummary.Size = new System.Drawing.Size(179, 13);
            this.lblSummary.TabIndex = 14;
            this.lblSummary.Text = "Вероятность суммарной ошибки: ";
            // 
            // Form
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(869, 401);
            this.Controls.Add(this.lblSummary);
            this.Controls.Add(this.lblFalsePositive);
            this.Controls.Add(this.lblErrorDetection);
            this.Controls.Add(this.btnSolve);
            this.Controls.Add(this.lblSecondDeviation);
            this.Controls.Add(this.tbSecondDeviation);
            this.Controls.Add(this.lblSecondMean);
            this.Controls.Add(this.tbSecondMean);
            this.Controls.Add(this.lblFirstDeviation);
            this.Controls.Add(this.tbFirstDeviation);
            this.Controls.Add(this.lblFirstMean);
            this.Controls.Add(this.tbFirstMean);
            this.Controls.Add(this.lblProbability);
            this.Controls.Add(this.tbProbability);
            this.Controls.Add(this.plot);
            this.Name = "Form";
            this.Text = "Form";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        private OxyPlot.WindowsForms.PlotView plot;

        #endregion

        private System.Windows.Forms.TextBox tbProbability;
        private System.Windows.Forms.Label lblProbability;
        private System.Windows.Forms.Label lblFirstMean;
        private System.Windows.Forms.TextBox tbFirstMean;
        private System.Windows.Forms.Label lblFirstDeviation;
        private System.Windows.Forms.TextBox tbFirstDeviation;
        private System.Windows.Forms.Label lblSecondMean;
        private System.Windows.Forms.TextBox tbSecondMean;
        private System.Windows.Forms.Label lblSecondDeviation;
        private System.Windows.Forms.TextBox tbSecondDeviation;
        private System.Windows.Forms.Button btnSolve;
        private System.Windows.Forms.Label lblErrorDetection;
        private System.Windows.Forms.Label lblFalsePositive;
        private System.Windows.Forms.Label lblSummary;
    }
}

